let data = [
	{
		id: 1,
		attributes: {
			openId: "644a4beae4b05bb94d42b24e",
			createdAt: "2023-08-15T01:36:40.274Z",
			updatedAt: "2023-08-18T09:58:36.599Z",
			publishedAt: "2023-08-15T01:37:06.923Z",
			nickname: null,
			menus: {
				data: [],
			},
			subTypes: {
				data: [],
			},
		},
	},
];

function transformData(data) {
	const finalResult: unknown[] = [];
	for (const item of data) {
		let result = flattenObj(item);
		//console.log(result);
		while (hasNestedObjectArray(result)) {
			result = flattenObj(result);
		}
		for (const key in result) {
			if (Object.hasOwnProperty.call(result, key)) {
				if (Array.isArray(result[key])) {
					const tempArray: unknown[] = [];
					for (const item of result[key]) {
						let temp = flattenObj(item);
						//console.log("第一次", temp);
						while (hasNestedObject(temp)) {
							//console.log("hhhh");
							temp = flattenObj(temp);
						}
						tempArray.push(temp);
					}
					result[key] = tempArray;
				}
			}
		}
		finalResult.push(result);
	}
	return finalResult;
}

// 调用方法
const finalResult = transformData(data);
console.log(finalResult);
for (const iterator of finalResult) {
	console.log(iterator);
	//console.log(iterator.subTypes);
}

// 判断对象里面是否还有data对象数组
function hasNestedObjectArray(obj) {
	for (const key in obj) {
		if (typeof obj[key] === "object" && obj[key] !== null) {
			if (
				Object.prototype.hasOwnProperty.call(obj[key], "data") &&
				Array.isArray(obj[key]["data"])
			) {
				return true; // 对象包含对象数组
			}
		}
	}
	return false; // 对象不包含对象数组
}

// 判断对象里面是否还有data对象
function hasNestedObject(obj) {
	for (const key in obj) {
		if (typeof obj[key] === "object" && obj[key] !== null) {
			if (
				Object.prototype.hasOwnProperty.call(obj[key], "data") &&
				typeof obj[key]["data"] === "object"
			) {
				return true; // 对象包含对象
			}
		}
	}
	return false; // 对象不包含其他对象
}

// 去除对象的data和attributes属性
function flattenObj(item) {
	//console.log(item);
	const flattened = {};
	for (const key in item) {
		//console.log(key);
		if (Object.hasOwnProperty.call(item, key)) {
			if (typeof item[key] === "object" && item[key] !== null) {
				// 有data元素时候
				if (Object.prototype.hasOwnProperty.call(item[key], "data")) {
					// data 是数组或者data是对象
					if (Array.isArray(item[key]["data"])) {
						flattened[key] = item[key]["data"];
					} else if (typeof item[key]["data"] === "object") {
						if (item[key]["data"] !== null) {
							flattened[key] = flattenObject(item[key]["data"]);
						} else {
							flattened[key] = null;
						}
					}
				} else {
					Object.assign(flattened, item[key]);
				}
			} else {
				flattened[key] = item[key];
			}
		}
	}
	return flattened;
}

// 对象去除attribute属性
function flattenObject(obj) {
	const flattened = {};
	for (const key in obj) {
		if (Object.prototype.hasOwnProperty.call(obj, key)) {
			if (typeof obj[key] === "object" && obj[key] !== null) {
				Object.assign(flattened, obj[key]);
			} else {
				flattened[key] = obj[key];
			}
		}
	}
	return flattened;
}

export { transformData };
