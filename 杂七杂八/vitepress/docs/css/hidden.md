---
title: css隐藏元素的7中方法
---

<style>
th {
  text-align: center !important;
}

tr td:nth-child(1) {
  width:228px !important;
  text-align: left !important;
  color:#000 !important;
}
</style>

# {{ $frontmatter.title }}

> 太阳当空照 花儿对我笑


#### 第一种
```js
display: none;
```

#### 第二种
```js
visibility: hidden;
```

#### 第三种
```js
opacity: 0;
```

#### 第四种
```js
transfrom: scale(0);
```

#### 第五种
```js
transfrom: translateX(-9999px);
```

#### 第六种
```js
clip-path: circle(0);
```

#### 第七种
```js
possition: absolute;
left: -9999px;
```

### 总结
|方法   | 屏幕阅读可见性 |占据空间 |键盘事件 |鼠标事件 |动画 |
|  :--:   |    :--:        |       :--:|      :--:|      :--:|   :--:|
|display:none;  |❌|❌|❌|❌|❌|
| visibility: hidden; |❌|✔️|❌|❌|❌|
| opacity: 0;|✔️|✔️|✔️|✔️|✔️|
|transfrom: scale(0);  |✔️|✔️|✔️|❌|✔️|
| transfrom: translateX(-9999px);|✔️|✔️|✔️|❌|✔️|
| clip-path: circle(0);|✔️|✔️|✔️|❌|✔️|
| possition: absolute; <br> left: -9999px; |✔️|❌|✔️|❌|✔️|








