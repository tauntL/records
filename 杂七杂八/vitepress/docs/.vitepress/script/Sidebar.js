// 二级页面
function sidebar() {
  return {
    '/css': [
      {
        text: '前端',
        collapsed: false,
        items: [
          { text: '前端助手', link: '/css/webtools' },
          { text: 'css隐藏元素的7中方法', link: '/css/hidden' }
        ]
      },
      {
        text: 'Group 2',
        collapsed: false,
        items: [
          { text: 'nihaio', link: '/css/one' },
          { text: 'fdsafdsa', link: '/css/two' }
        ]
      }
    ],

    '/js': [
      {
        text: 'Group 1',
        collapsed: false,
        items: [
          { text: 'nihaio', link: '/js/one' },
          { text: 'fdsafdsa', link: '/js/two' }
        ]
      },
      {
        text: 'Group 2',
        collapsed: false,
        items: [
          { text: 'nihaio', link: '/js/one' },
          { text: 'fdsafdsa', link: '/js/two' }
        ]
      }
    ]
  };
}

export default sidebar;
