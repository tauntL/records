// 主页面导航

function nav() {
  return [
    { text: "🏚️主页", link: "/" },
    { text: "CSS", link: "/css/webtools", activeMatch: "/css" },
    { text: "JS", link: "/js/one", activeMatch: "/js" },
  ];
}

export default nav;
