import DefaultTheme from 'vitepress/theme';
import Layout from './Layout.vue';

import './custom.css';
// import './style.css'; //紫色风格

export default {
  ...DefaultTheme,
  Layout,

  enhanceApp({ app, router, siteData }) {}
};
