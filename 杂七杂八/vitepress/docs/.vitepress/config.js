import { defineConfig } from "vitepress";

import nav from "./script/Nav";

import sidebar from "./script/Sidebar";

import search from "./script/Search";

import lastUpdated from "./script/LastTime";

export default defineConfig({
  base: "./",
  outDir: "dist",
  lang: "zh-CN",

  title: "MY BLOG",

  description: "MY BLOG",

  head: [["link", { rel: "icon", href: "/favicon.ico" }]], //设置ico

  cleanUrls: true, //是否启用干净的URL，例如/about代替/about.html。

  themeConfig: {
    logo: "/logo.png",

    copyCode: {
      buttonText: "复制代码", // 在这里更改按钮文字
      errorText: "复制失败", // 更改复制失败时的提示文字
      successText: "已复制", // 更改成功复制时的提示文字
    },

    outline: [2, 4],

    outlineTitle: "本页目录",

    sidebarMenuLabel: "菜单",

    returnToTopLabel: "返回顶部",

    search: search(), // 搜索配置

    nav: nav(), // 导航配置

    sidebar: sidebar(), // 二级导航配置

    // 文章底部链接
    editLink: {
      pattern: "https://github.com/vuejs/vitepress/edit/main/docs/:path",
      text: "在GitHub上编辑此页面",
    },

    // 右上角图标链接
    socialLinks: [
      { icon: "github", link: "https://github.com/vuejs/vitepress" },
    ],

    lastUpdated: lastUpdated(), // 允许自定义最后更新文本和日期格式。

    docFooter: { prev: "上一页", next: "下一页" }, // 自定义上一个和下一个链接上方显示的文本
  },
});
