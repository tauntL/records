import * as sass from "sass";

self.onmessage = (event) => {
  const scssCode = event.data;
  try {
    const result = sass.compileString(scssCode);
    self.postMessage({ compiledCss: result.css });
  } catch (error: any) {
    self.postMessage({ error: error.message });
  }
};
