self.onmessage = (event) => {
  const num = event.data;
  try {
    let result = [];
    for (let i = 0; i <= num; i++) {
      if (i % 2 === 0) {
        result.push(i);
      } else {
        result.push("奇数位");
      }
    }
    self.postMessage({ result });
  } catch (error: any) {
    self.postMessage({ error: error.message });
  }
};
