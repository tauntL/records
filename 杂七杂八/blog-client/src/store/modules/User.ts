import { defineStore } from "pinia";
import { Storage } from "@/utils/Storage";
import { loginByTicket, ssoGetuserInfo } from "@/axios/api/login";

export const useUserStore = defineStore("userStore", {
  state: () => {
    return {
      username: "",
      sessiontoken: "",
      accesstoken: "",
      platform: "http://192.168.1.127:8100/",
      clientId: "64bf4aa0e4b09b98d4d17c46",
      responseType: "code",
    };
  },
  getters: {},
  actions: {
    /** 登录成功保存token */
    setToken(
      tokenName: string,
      tokenValue: string,
      tokenTimeout: number = 1 * 24 * 60 * 60
    ) {
      this.sessiontoken = tokenValue ?? "";
      // const ex = 1 * 24 * 60 * 60 * 1000;
      Storage.set("TOKEN_VALUE", this.sessiontoken, tokenTimeout);
      Storage.set("TOKEN_NAME", tokenName, tokenTimeout);
    },
    /** 根据ticket登录 */
    async loginByTicket(ticket: string): Promise<any> {
      loginByTicket(ticket).then(async (res: any) => {
        this.setToken(
          res.data.tokenName,
          res.data.tokenValue,
          res.data.tokenTimeout
        );

        const [ssoUserInfo] = await Promise.all([ssoGetuserInfo()]);

        Object.keys(ssoUserInfo).map((item: string) => {
          console.log(this);

          // this[item] = ssoUserInfo[item];
        });

        // this.setAccessToken(tokenInfo.access_token, tokenInfo.expiresIn);
        return Promise.resolve(true);
      });
    },
  },
});
