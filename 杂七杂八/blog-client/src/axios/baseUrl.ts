/** 网络请求基础地址 */
const APP_BASE_API = "http://61.149.143.246:81/ocr-display/";

/** strapi请求基础地址 */
const APP_BASE_STRAPI = "http://39.98.84.181:8088/";

/** OCR相关请求基础地址 */
const BASE_API = "http://61.149.143.246:81/web-api/";

/** 文字识别基础地址 */
// const APP_REC_API= `${BASE_API}ocr-api/`
const APP_REC_API = `http://61.149.143.246:8918/`;

/** 图像处理基础地址 */
// const APP_IMG_API=  `${BASE_API}image-api/`
const APP_IMG_API = `http://61.149.143.246:8919/`;

/** 文档处理基础地址 */
// const APP_DOC_API=`${BASE_API}document-api/`
const APP_DOC_API = `http://61.149.143.246:8918/`;

export {
  APP_BASE_API,
  BASE_API,
  APP_BASE_STRAPI,
  APP_REC_API,
  APP_IMG_API,
  APP_DOC_API,
};
