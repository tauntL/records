import axios, { AxiosRequestConfig, AxiosResponse } from "axios";
import { APP_BASE_API } from "./baseUrl.ts";
import { Storage } from "@/utils/Storage";

// 创建axios实例
const request = axios.create({
  // @ts-ignore
  baseURL: APP_BASE_API,
  timeout: 5000, // 请求超时时间
});

// 请求拦截器
request.interceptors.request.use(
  // @ts-ignore
  (config: AxiosRequestConfig) => {
    const tokenValue = Storage.get("TOKEN_VALUE");
    const tokenName = Storage.get("TOKEN_NAME");
    if (tokenValue && config.headers) {
      // 请求头token信息，请根据实际情况进行修改
      config.headers[tokenName] = tokenValue;
    }
    return config;
  },
  (error: any) => {
    // 对请求错误做些什么
    return Promise.reject(error);
  }
);

// 响应拦截器
request.interceptors.response.use(
  (response: AxiosResponse) => {
    // 对响应数据做些什么
    const res = response.data;
    if (res.code !== 0) {
      // 自定义错误处理
      return Promise.reject(new Error(res.message || "Error"));
    } else {
      return res;
    }
  },
  (error: any) => {
    // 对响应错误做些什么
    return Promise.reject(error);
  }
);

// 导出封装的axios请求
export default request;
