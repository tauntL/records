import request from "../request.ts";

export function fetchData() {
  return request({
    url: "/api/data",
    method: "get",
  });
}

export function login() {
  return request({
    url: "/api/data",
    method: "get",
  });
}
// 通过SessionToken获取AccessToken

export function getAccessToken() {
  return request({
    url: `oauth2/accessToken`,
    method: "get",
  });
}
/**
 * @description 返回SSO认证中心登录地址
 */
export function getIAMAddress(clientLoginUrl: string) {
  return request<Response>({
    url: `sso/serverAuthUrl?clientLoginUrl=${clientLoginUrl}`,
    method: "get",
  });
}

/**
 * @description 单点登录获取用户信息
 */
export function ssoGetuserInfo() {
  return request({
    url: `sso/userinfo`,
    method: "get",
  });
}

/**
 * @description 根据ticket登录
 * @param {LoginParams} data
 * @returns
 */
export function loginByTicket(ticket: string) {
  return request<Response>({
    url: `sso/login/ticket?ticket=${ticket}`,
    method: "post",
  });
}
/**
 * OCR识别函数
 * @param uploadParams 上传参数
 * @param bodyData body参数
 * @param config 其他配置
 * @return {Promise<unknown>}
 */
export const recognize = (url, headers?, data?, config?) => {
  return request({
    url,
    method: "post",
    data,
    ...config,
    headers: {
      ...headers,
      "content-Type": "application/x-www-form-urlencoded",
    },
  });
};
