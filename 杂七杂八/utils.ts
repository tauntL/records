/**
 * 防抖
 * @param {Function} fn 函数
 * @param {Number} delay 延迟
 */
export function debounce(fn, delay) {
    let timer;
    return function() {
        if (timer) {
            clearTimeout(timer);
        }
        timer = setTimeout(fn, delay);
    };
}

/**
 * 节流
 * @param {Function} fn 函数
 * @param {Number} delay 延迟
 */
export function throttle(fn, delay) {
    let valid = true;
    return function() {
        if (!valid) {
            return false;
        }
        valid = false;
        setTimeout(() => {
            fn();
            valid = true;
        }, delay);
    };
}