import { createRouter, createWebHistory } from "vue-router";

let routes = [
  {
    path: "/",
    name: "layout",
    redirect: "/dashboard",
    component: () => import("@/layout/index.vue"),
    children: [
      {
        path: "/dashboard",
        name: "dashboard",
        component: () => import("@/views/dashboard/index.vue"),
      },
      {
        path: "/blogList",
        name: "blogList",
        component: () => import("@/views/blogList/index.vue"),
      },
      {
        path: "/blogList/:id",
        name: "blogEdit",
        component: () => import("@/views/blogList/component/blogEdit.vue"),
      },

      {
        path: "/setting",
        name: "setting",
        component: () => import("@/views/setting/index.vue"),
      },
    ],
  },
  {
    path: "/login",
    name: "login",
    component: () => import("@/views/login/index.vue"),
  },

  {
    path: "/404",
    name: "404",
    component: () => import("@/views/404/index.vue"),
  },
  {
    path: "/:catchAll(.*)",
    name: "Any",
    redirect: "/404",
  },
];
// 路由
const router = createRouter({
  history: createWebHistory(),
  routes,
  scrollBehavior() {
    return {
      left: 0,
      top: 0,
    };
  },
});
// 导出
export default router;
