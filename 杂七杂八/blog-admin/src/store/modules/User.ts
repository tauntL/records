import { defineStore } from "pinia";
import { Storage } from "@/utils/Storage";
import { login } from "@/axios/api/login";

export const useUserStore = defineStore("userStore", {
  state: () => {
    return {
      username: "",
      sessiontoken: "",
      accesstoken: "",
      platform: "http://192.168.1.127:8100/",
      clientId: "64bf4aa0e4b09b98d4d17c46",
      responseType: "code",
    };
  },
  getters: {},
  actions: {
    /** 登录成功保存token */
    setToken(
      tokenName: string,
      tokenValue: string,
      tokenTimeout: number = 1 * 24 * 60 * 60
    ) {
      this.sessiontoken = tokenValue ?? "";

      Storage.set("TOKEN_VALUE", this.sessiontoken, tokenTimeout);
      Storage.set("TOKEN_NAME", tokenName, tokenTimeout);
    },
    /** 登录 */
    async login(data: { username: string; password: string }): Promise<any> {
      try {
        const res = await login(data);
        this.setToken(
          res.data.tokenName,
          res.data.tokenValue,
          res.data.tokenTimeout
        );
        return Promise.resolve(true);
      } catch {
        return Promise.reject();
      }
    },
  },
});
