import request from "../request.ts";

/** 分页查询文章列表 */
export function getArticlesByPage(data: any) {
  return request({
    url: "/api/articles",
    method: "get",
    data,
  });
}

/** 根据文章ID查询文章内容 */
export function getArticlesDetails(id: string) {
  return request({
    url: `/api/articles/${id}`,
    method: "get",
  });
}

/** 发布文章 */
export function publishArticle(data: any) {
  return request({
    url: "/api/articles",
    method: "post",
    data,
  });
}

/** 删除文章 */
export function deleteArticle(id: string) {
  return request({
    url: `/api/articles/${id}`,
    method: "delete",
  });
}

/** 修改文章 */
export function updateArticle(data: any) {
  return request({
    url: `/api/articles/${data.id}`,
    method: "put",
    data,
  });
}
