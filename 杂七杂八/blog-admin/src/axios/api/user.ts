import request from "../request.ts";

/** 查询作者列表 */
export function getAuthors() {
  return request({
    url: "/api/users/list",
    method: "get",
  });
}
