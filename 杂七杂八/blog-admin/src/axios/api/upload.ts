import request from "../request.ts";

/** 文件上传 */
export function uploadFile(data) {
  return request({
    url: "/api/files/upload",
    method: "post",
    data,
  });
}
