import request from "../request.ts";

export function login(data: { username: string; password: string }) {
  return request({
    url: "/api/users/login",
    method: "post",
    data,
  });
}

export function register(data: any) {
  return request({
    url: "/api/users/register",
    method: "post",
    data,
  });
}
