const mongoose = require("mongoose");

mongoose
  .connect("mongodb://127.0.0.1/blogDB")
  .then((res) => {
    // console.log(res);
    console.log("数据库连接成功");
  })
  .catch((err) => {
    console.log(err);
  });

const Schema = mongoose.Schema;
//定义用户结构
const UserSchema = new Schema(
  {
    username: {
      type: String,
      unique: true,
      required: true,
    }, //用户名
    nickname: String, //昵称
    password: String, //密码
    avatar: String, //头像
  },
  {
    timestamps: true,
  }
);
//定义文章结构
const ArticleSchema = new Schema(
  {
    title: String, //文章标题
    abstract: String, //文章摘要
    content: String, //文章内容
    tag: String, //文章分类
    titleImageUrl: String, //文章标题图片
    author: {
      type: Schema.Types.ObjectId,
      ref: "User", //关联用户表
    }, //文章作者
    views: {
      type: Number,
      default: 0, //浏览数
    },
    agree: {
      type: Number,
      default: 0, //点赞数
    },
    disagree: {
      type: Number,
      default: 0, //踩数
    },
  },
  {
    timestamps: true,
  }
);
ArticleSchema.virtual("comments", {
  ref: "Comment",
  localField: "_id",
  foreignField: "article_id",
  justOne: false, //取Array值，会把文章对应的评论全部提取出来
  // count:true//取总数，true:只显示数组长度，不显示数组内容
});
//执行下面两个命令虚拟字段才能看到
ArticleSchema.set("toObject", { virtuals: true });
ArticleSchema.set("toJSON", { virtuals: true });

//定义评论结构
const CommentSchema = new Schema(
  {
    content: String, //内容
    article_id: {
      type: Schema.Types.ObjectId,
      ref: "Article", //关联文章表
    },
    reply_user_id: {
      type: Schema.Types.ObjectId,
      ref: "User", //关联用户表
    },
  },
  {
    timestamps: true,
  }
);

//创建文章数据模型
const User = mongoose.model("User", UserSchema);
const Article = mongoose.model("Article", ArticleSchema);
const Comment = mongoose.model("Comment", CommentSchema);

//导出
module.exports = {
  User,
  Article,
  Comment,
};
// User.create([
//   {
//     username: "test",
//     nickname: "测试员",
//     password: "123456",
//     avatar: "",
//   },
//   {
//     username: "admin",
//     nickname: "管理员",
//     password: "123456",
//     avatar: "",
//   },
// ]).then((res) => {
//   console.log(res);
//   console.log("插入数据成功");
// });
//数据模型创建并插入数据
//方法一:可以传入一个对象或者数组
// Article.create([
//   {
//     title: "使用命令快速创建vue3项目",
//     content: "使用命令快速创建vue3项目",
//     tag: "vue",
//     author: "Lee",
//   },
//   {
//     title: "vue3中的全局状态管理",
//     content: "vue3中的全局状态管理--pinia",
//     tag: "vue",
//     author: "Lee",
//   },
// ]).then((res) => {
//   console.log(res);
//   console.log("插入数据成功");
// });
//方法二
// const article = new Article({
//   title: "save test",
//   content: "使用save手动保存数据到数据库",
//   tag: "vue",
//   author: "Lee",
// });
// article.save().then((res) => {
//   console.log(res);
//   console.log("save数据成功");
// });

//数据模型删除数据
//删除单条
// Article.deleteOne({ _id: "650d38bcf33699813a27d4db" }).then((res) => {
//   console.log(res);
//   console.log("删除单条成功");
// });
//删除多条
// Article.deleteMany({ content: /vue/ }).then((res) => {
//   console.log(res);
//   console.log("删除多条成功");
// });

//数据模型修改数据
//修改单条数据
// Article.updateOne(
//   { _id: "650d3aae53836cd3e274327e" },
//   { title: "测试一下修改功能" }
// ).then((res) => {
//   console.log(res);
//   console.log("修改单条成功");
// });
// Article.updateOne(
//   { _id: "650d3aae53836cd3e274327e" },
//   {
//     $inc: { views: 1 },
//   }, //views自增1
//   { timestamps: false } //不更改更新时间
// ).then((res) => {
//   console.log(res);
//   console.log("修改单条成功");
// });
//修改多条数据
// Article.updateMany(
//   { tag: "vue" },
//   { content: "vue2中的全局状态管理--vuex vue3中的全局状态管理--pinia" }
// ).then((res) => {
//   console.log(res);
//   console.log("修改多条成功");
// });

//数据模型查询数据
//根据id查询一条数据
// Article.findById("650d3aae53836cd3e274327e").then((res) => {
//   console.log(res);
//   console.log("根据id查询一条数据成功");
// });
//根据id查询并且更新数据
//Article.findByIdAndUpdate
// Article.findByIdAndRemove
// Article.findByIdAndDelete
// Article.findByIdAndUpdate(
//   "650d3aae53836cd3e274327e",
//   {
//     $inc: { views: 1 },
//   }, //views自增1
//   { timestamps: false } //不更改更新时间
// ).then((res) => {
//   console.log(res);
//   console.log("根据id查询一条数据并更改成功");
// });
//查询多条数据
// Article.find({
//   views: { $gte: 0, $lt: 1000 },
//   //   title: /测试/,
// })
//   .sort({ _id: -1 }) //根据id排序 1:升序 -1:降序
//   .skip(0) //跳过前0条
//   .limit(10) //获取10条
//   .select({ author: 0 }) //查询结果中出现某个字段 1:出现 0:不出现
//   .populate("author", { password: 0 }) //关联查询，不显示password
//   .populate("comments", {}) //
//   .exec() //执行查询
//   .then((res) => {
//     console.log(res);
//     console.log("查询成功");
//   });
