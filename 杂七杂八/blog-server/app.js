const createError = require("http-errors"); //处理错误的插件
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser"); //解析cookie
const logger = require("morgan"); //日志插件
const { expressjwt } = require("express-jwt");

const indexRouter = require("./routes/index");
const usersRouter = require("./routes/users");
const articlesRouter = require("./routes/articles");
const filesRouter = require("./routes/files");
const commentsRouter = require("./routes/comments");

const app = express();

// view engine setup 模板引擎
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.all("*", (req, res, next) => {
  res.header("Access-Control-Allow-Origin", req.headers.origin || "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Content-Type,Authorization,X-Requested-With"
  );
  //跨域允许的请求方式
  res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS"); // 可以带cookies
  res.header("Access-Control-Allow-Credentials", true);
  if (req.method == "OPTIONS") {
    res.sendstatus(200).end();
  } else {
    next();
  }
});
//校验token
app.use(
  expressjwt({
    secret: "testPrivateKey146438",
    algorithms: ["HS256"],
  }).unless({
    path: [
      "/",
      {
        url: /\/favicon.ico$/,
        methods: ["GET"],
      },
      "/api/users/register",
      "/api/users/login",
      //动态路由需要使用正则匹配 w+:数字字符下划线
      /^\/api\/articles\/users\/\w+/,
      {
        url: /^\/api\/articles\/\w+/,
        methods: ["GET"],
      },
    ],
  })
);
//挂载路由
app.use("/", indexRouter);
app.use("/api/users", usersRouter);
app.use("/api/articles", articlesRouter);
app.use("/api/files", filesRouter);
app.use("/api/comments", commentsRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  if (err) {
    res.json({
      code: 11011,
      message: err.message || "未知错误",
    });
    return;
  }
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
