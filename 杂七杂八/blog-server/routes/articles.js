var express = require("express");
var router = express.Router();
const { Article } = require("../models/index");

/** 发布文章 */
router.post("/", function (req, res, next) {
  console.log(req.body);
  req.body.tag = req.body.tag.join("");
  Article.create({
    ...req.body,
    author: req.auth.id,
  })
    .then((data) => {
      res.json({
        code: 0,
        message: "发布成功",
      });
    })
    .catch((err) => {
      res.json({
        code: err.code,
        message: err.message || "发布失败",
      });
    });
});

/** 编辑文章 */
router.put("/:articleId", function (req, res, next) {
  console.log(req.body);
  req.body.tag = req.body.tag.join(",");
  Article.findByIdAndUpdate(
    req.params.articleId,
    { ...req.body },
    {
      new: true,
    }
  )
    .then((data) => {
      if (data) {
        res.json({
          code: 0,
          data,
          message: "更新成功",
        });
      } else {
        res.json({
          code: 1,
          message: "文章不存在!",
        });
      }
    })
    .catch((err) => {
      res.json({
        code: err.code || 1,
        message: err.message || "文章更新失败",
      });
    });
});

/** 删除文章 */
router.delete("/:articleId", function (req, res, next) {
  console.log(req.body);
  Article.findByIdAndDelete(req.params.articleId)
    .then((data) => {
      if (data) {
        res.json({
          code: 0,
          data,
          message: "删除成功",
        });
      } else {
        res.json({
          code: 1,
          message: "文章不存在!",
        });
      }
    })
    .catch((err) => {
      res.json({
        code: err.code || 1,
        message: err.message || "删除失败",
      });
    });
});

/** 根据用户id获取某个用户的文章列表 */
router.get("/users/:userId", function (req, res, next) {
  console.log(req.body);
  Article.find({ author: req.params.userId }, { content: 0 })
    .populate("author", { password: 0 })
    .populate("comments")
    .then((data) => {
      res.json({
        code: 0,
        data,
        message: "获取成功",
      });
    })
    .catch((err) => {
      res.json({
        code: err.code || 1,
        message: err.message || "获取失败",
      });
    });
});

/** 分页查询文章列表 */
router.get("/", function (req, res, next) {
  console.log(req.body);
  const { current = 1, size = 10 } = req.body;

  Article.find()
    .sort({ updataAt: -1 })
    .skip((current - 1) * 10)
    .limit(size)
    .select({ content: 0 })
    .populate("author", { password: 0 })
    .populate("comments")
    .then((data) => {
      res.json({
        code: 0,
        data,
        message: "获取成功",
      });
    })
    .catch((err) => {
      res.json({
        code: err.code || 1,
        message: err.message || "获取失败",
      });
    });
});

/** 获取文章详情 */
router.get("/:articleId", function (req, res, next) {
  console.log(req.body);
  Article.findByIdAndUpdate(
    req.params.articleId,
    { $inc: { views: 1 } },
    { new: true, timestamps: false }
  )
    .populate("author", { password: 0 })
    .populate("comments")
    .then((data) => {
      res.json({
        code: 0,
        data,
        message: "获取成功",
      });
    })
    .catch((err) => {
      res.json({
        code: err.code || 1,
        message: err.message || "获取失败",
      });
    });
});

module.exports = router;
