const express = require("express");
const multer = require("multer");
const md5 = require("md5");
const path = require("path");
const os = require("os");

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    console.log(file);
    console.log(typeof file);
    cb(null, "public/images");
  },
  filename: (req, file, cb) => {
    console.log(typeof file);
    cb(null, Date.now() + path.extname(file.originalname));
  },
});
// .single('img') 支持一次上传一张图片,参数名:img 参数值:图片
const upload = multer({ storage: storage }).single("img");

router.post("/upload", upload, function (req, res, next) {
  console.log(req.file);
  let ip = getLocalIp();
  console.log("ip: ", ip);
  const file = req.file;
  const url = `http://${ip}:${process.env.PORT}/images/${file.filename}`;
  res.json({
    code: 0,
    data: {
      url,
    },
    message: "上传成功",
  });
});

router.post("/download", function (req, res, next) {
  console.log(req.body);
  res.send("respond with a resource");
});
//获取本机ip
function getLocalIp() {
  const networkInterfaces = os.networkInterfaces();
  console.log("networkInterfaces: ", networkInterfaces);
  let ip = "";
  Object.values(networkInterfaces).forEach((list) => {
    list.forEach((ipInfo) => {
      if (
        ipInfo.family === "IPv4" &&
        ipInfo.address !== "127.0.0.1" &&
        !ipInfo.internal
      ) {
        ip = ipInfo.address;
      }
    });
  });
  return ip;
}

module.exports = router;
