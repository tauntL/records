var express = require("express");
var router = express.Router();
const { Comment } = require("../models/index");

/** 根据文章id获取某篇文章的评论列表 */
router.get("/:articleId", (req, res, next) => {
  Comment.find({ article_id: req.params.articleId })
    .populate("reply_user_id", { password: 0 })
    .then((data) => {
      res.json({
        code: 0,
        message: "成功",
        data,
      });
    })
    .catch((err) => {
      res.json({
        code: err.code,
        message: err.message || "未知错误",
      });
    });
});

/** 发表评论 */
router.post("/", function (req, res, next) {
  console.log(req.body);
  console.log(req.auth);
  Comment.create({
    reply_user_id: req.auth.id,
    article_id: req.body.article_id,
    content: req.body.content,
  })
    .then((r) => {
      if (!r) {
        res.json({
          code: 1,
          message: err.message || "未知错误",
        });
      } else {
        res.json({
          code: 0,
          message: "发布成功",
        });
      }
    })
    .catch((err) => {
      console.log(err);
      res.json({
        code: err.code,
        message: err.message || "未知错误",
      });
    });
});

/** 编辑评论 */
router.put("/", function (req, res, next) {
  console.log(req.body);
  res.json({
    code: 0,
    data: "",
    message: "编辑成功",
  });
});

/** 根据评论id删除评论 */
router.delete("/commentId", async function (req, res, next) {
  console.log(req.body);
  const commentObj = await Comment.findById(req.params.commentId).populate(
    "article_id"
  );
  const author_id = commentObj.article_id._id;
  if (author_id === req.auth.id) {
    const data = await Comment.findByIdAndDelete(req.params.commentId);
    if (data) {
      res.json({
        code: 0,
        message: "删除成功",
      });
    } else {
      res.json({
        code: 1,
        message: "删除已经被删除",
      });
    }
  } else {
    res.json({
      code: 0,
      message: "删除失败",
    });
  }
});

module.exports = router;
