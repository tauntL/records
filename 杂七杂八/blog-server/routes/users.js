const express = require("express");
const jwt = require("jsonwebtoken");
const { User } = require("../models/index");

const router = express.Router();

/** 登录 */
router.post("/login", function (req, res, next) {
  console.log(req.body);
  const { username, password } = req.body;
  if (!username || !password) {
    res.json({
      code: 1,
      message: "登录失败",
    });
    return;
  }
  User.findOne(req.body)
    .then((data) => {
      console.log(data);
      if (!data) {
        res.json({
          code: 1,
          message: "用户名或密码错误!",
        });
      } else {
        const token = jwt.sign(
          { username, id: data._id },
          "testPrivateKey146438",
          {
            expiresIn: "7d",
            algorithm: "HS256",
          }
        );
        res.json({
          code: 0,
          data: {
            tokenValue: token,
            tokenName: "Authorization",
            tokrnTimeout: 86400,
            id: data._id,
            username: data.username,
            nickname: data.nickname,
            avatar: data.avatar,
          },
          message: "登录成功",
        });
      }
    })
    .catch((err) => {
      res.json({
        code: 1,
        message: err.message || "未知错误",
      });
    });
});

/** 注册 */
router.post("/register", function (req, res, next) {
  console.log(req.body);
  const { username, password, nickname } = req.body;
  if (!username || !password) {
    res.json({
      code: 1,
      message: "缺少必填信息,注册失败!",
    });
  } else {
    User.create(req.body)
      .then(() => {
        res.json({
          code: 0,
          message: "注册成功!",
        });
      })
      .catch((err) => {
        console.log(err);
        res.json({
          code: err.code,
          message: "用户名已存在!",
        });
      });
  }
});

/** 获取用户列表 */
router.get("/list", function (req, res, next) {
  console.log(req.body);

  User.find({}, { password: 0 })
    .then((data) => {
      console.log(data);

      res.json({
        code: 0,
        data,
        message: "成功",
      });
    })
    .catch((err) => {
      res.json({
        code: 1,
        message: err.message || "未知错误",
      });
    });
});

module.exports = router;
