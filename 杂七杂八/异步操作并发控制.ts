//   // 模拟请求 0.5概率成功，随机产生100-500ms延时
//   function randomRequest(url) {
//     return new Promise((resolve, reject) => {
//       let delay = Math.floor(Math.random() * 2000 + 100);
//       setTimeout(() => {
//         let rand = Math.random();
//         if (rand > 0.5) resolve({ state: "success", data: { url } });
//         else reject({ state: "error" });
//       }, delay);
//     });
//   }

//   // 并发控制函数
//   const controlAsync = (urls, maxNum) => {
//     return new Promise((resolve) => {
//       if (urls.length === 0) {
//         resolve([]);
//         return;
//       }
//       const results = [];
//       let index = 0; // 下一个请求的下标
//       let count = 0; // 当前请求完成的数量

//       // 发送请求
//       async function request() {
//         if (index === urls.length) return;
//         const i = index; // 保存序号，使result和urls相对应
//         const url = urls[index];
//         index++;
//         console.log(url);
//         try {
//           const resp = await randomRequest(url);
//           // resp 加入到results
//           results[i] = resp;
//         } catch (err) {
//           // err 加入到results
//           results[i] = err;
//         } finally {
//           count++;
//           // 判断是否所有的请求都已完成
//           if (count === urls.length) {
//             console.log("完成了");
//             resolve(results);
//           }
//           request();
//         }
//       }
//       // maxNum和urls.length取最小进行调用
//       const times = Math.min(maxNum, urls.length);
//       for (let i = 0; i < times; i++) {
//         request();
//       }
//     });
//   };

//   const urls = [];
//   for (let i = 1; i <= 20; i++) {
//     urls.push(`https://xxx.xx/api/${i}`);
//   }
//   controlAsync(urls, 3).then((res) => {
//     console.log(res);
//   });

//带并发控制的Promise异步调度器
class Scheduler {
  max: number;
  work: any[];
  unwork: any[];

  constructor() {
    this.max = 2; // 最大限制数
    this.work = []; // 正在执行的任务盒子
    this.unwork = []; // 等待的任务盒子
  }
  add(promise) {
    if (this.work.length < this.max) {
      //还没达到最大数量限制，可以直接执行
      this.runTask(promise);
    } else {
      // 此时任务盒子数量达到并发最大数量，那就放在等待区域
      this.unwork.push(promise);
    }
  }
  runTask(promise) {
    this.work.push(promise);
    promise()
      .then((res) => {
        console.log(res);
      })
      .finally(() => {
        // 任务执行完毕，就立马从执行任务盒子删掉
        const index = this.work.indexOf(promise);
        this.work.splice(index, 1); // 如果任务等待盒子还有任务，那就继续执行
        if (this.unwork.length) {
          this.runTask(this.unwork.shift());
        }
      });
  }
}

const scheduler = new Scheduler();
const timeout = (time, order) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(order);
    }, time);
  });
};
const addTask = (time, order) => {
  scheduler.add(() => timeout(time, order));
};
addTask(4000, 1);
addTask(1000, 2);
addTask(900, 3);
addTask(3000, 4);
