# 这是使用 VitePress 搭建的模板网站

## 里面提供了基本配置，方便开发文档、个人博客的快速搭建

### 使用方法

- 将项目克隆到您的本地

```bash
https://gitee.com/HikJ/vitepress-template.git
```

- 使用您喜欢的包管理器下载依赖

> 使用 npm

```bash
$ npm i
```

> 使用 yarn

```bash
$ yarn i
```

> 使用 pnpm

```bash
$ pnpm i
```

- 启动该项目

```bash
$ pnpm docs:dev
```

<img src="./docs/public/index.png"/>

### 至此，您已经完成了项目的初始化，利用里面提供的配置信息来快速搭建属于自己的网站。项目搭建完成后，使用如下命令来打包，将打包好的文件发布到服务器或代码托管平台进行上线

```bash
$ pnpm docs:build
```
