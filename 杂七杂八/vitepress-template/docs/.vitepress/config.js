export default {
  // 网站的标题
  title: "My Blog",
  // 标题模板
  titleTemplate: "blog template",
  // 网站说明。这将在页面 HTML 中呈现为 <meta> 标记。
  description: "A VitePress Site",
  // 网页页签图标
  head: [["link", { rel: "icon", href: "/favicon.ico" }]],
  // 网站的 lang 属性
  lang: "zh-CN",
  // 将在其中部署站点的基本 URL。如果您计划在子路径（例如 GitHub 页面）下部署站点，则需要设置此设置。如果计划将站点部署到 https://foo.github.io/bar/ ，则应将 base 设置为 '/bar/'
  // base: '/bar/',
  // 上次更新时间戳
  lastUpdated: true,
  //是否启用干净的URL，例如/about代替/about.html
  cleanUrls: true,
  themeConfig: {
    // 网站标题的 logo
    logo: "/logo.png",
    // 导航
    nav: [
      { text: "菜单1", link: "/menu1/one", activeMatch: "/menu1/" },
      {
        text: "菜单2",
        items: [
          {
            text: "菜单2",
            items: [
              { text: "Item 1", link: "/menu2/item1/a" },
              { text: "Item 2", link: "/menu2/item2/c" },
            ],
          },
        ],
      },
      {
        text: "菜单3",
        items: [
          {
            items: [
              {
                text: "github",
                link: "https://github.com/a422916528",
                target: "_blank",
                rel: "sponsored",
              },
              {
                text: "gitee",
                link: "https://gitee.com/HikJ",
                target: "_blank",
                rel: "sponsored",
              },
            ],
          },
        ],
      },
    ],
    // 侧边栏
    sidebar: {
      // 菜单1
      "/menu1/": [
        {
          text: "菜单A",
          collapsed: false,
          items: [
            { text: "One", link: "/menu1/one" },
            { text: "Two", link: "/menu1/two" },
          ],
        },
        {
          text: "菜单B",
          collapsed: false,
          items: [
            { text: "Three", link: "/menu1/three" },
            { text: "Four", link: "/menu1/four" },
          ],
        },
      ],
      // 菜单2-item1
      "/menu2/item1/": [
        {
          text: "item1",
          collapsed: true,
          items: [
            { text: "a", link: "/menu2/item1/a" },
            { text: "b", link: "/menu2/item1/b" },
          ],
        },
      ],
      // 菜单2-item2
      "menu2/item2/": [
        {
          text: "item2",
          collapsed: true,
          items: [
            { text: "c", link: "/menu2/item2/c" },
            { text: "d", link: "/menu2/item2/d" },
          ],
        },
      ],
    },
    // 页脚，侧边栏可见时不会显示
    footer: {
      message: "",
      copyright: 'Copyright © 2023-present <a href="">Tornado</a>',
    },
    // 编辑链接
    editLink: {
      pattern: "https://gitee.com/HikJ/vitepress-template",
      text: "在 Gitee 上编辑此页",
    },
    // 本地搜索
    search: {
      provider: "local",
    },
    // 社交链接
    socialLinks: [
      /*
       可选图标
        'discord'
        'facebook'
        'github'
        'instagram'
        'linkedin'
        'mastodon'
        'slack'
        'twitter'
        'youtube'
			 */
      { icon: "github", link: "https://gitee.com/HikJ" },
      // 还可以使用 SVG :
      //   {
      //     icon: {
      //       svg: "",
      //     },
      //     link: "...",
      //   },
    ],
  },
};
