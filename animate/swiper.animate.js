/** swiper中文网提供的swiper.animate.min.js改写版本 */
export function swiperAnimateCache(a) {
    for (let j = 0; j < a.slides.length; j++)
        for (
            let allBoxes = a.slides[j].querySelectorAll('.ani'), i = 0; i < allBoxes.length; i++
        )
            if (allBoxes[i].attributes.style) {
                allBoxes[i].setAttribute(
                    'swiper-animate-style-cache',
                    allBoxes[i].attributes.style.value
                )
            } else {
                allBoxes[i].setAttribute('swiper-animate-style-cache', ' ')
                allBoxes[i].style.visibility = 'hidden'
            }
}
export function swiperAnimate(a) {
    clearSwiperAnimate(a)
    const b = a.slides[a.activeIndex].querySelectorAll('.ani')
    for (let i = 0; i < b.length; i++) {
        b[i].style.visibility = 'visible'
        let effect
        if (b[i].attributes['swiper-animate-effect']) {
            effect = b[i].attributes['swiper-animate-effect'].value
        } else {
            effect = ''
        }
        b[i].className = b[i].className + '  animate__' + effect + ' ' + 'animate__animated'
        let style = b[i].attributes.style.value
        let duration
        if (b[i].attributes['swiper-animate-duration']) {
            duration = b[i].attributes['swiper-animate-duration'].value
        } else {
            duration = ''
        }
        if (duration) {
            style =
                style +
                'animation-duration:' +
                duration +
                ';-webkit-animation-duration:' +
                duration +
                ';'
        }
        let delay
        if (b[i].attributes['swiper-animate-delay']) {
            delay = b[i].attributes['swiper-animate-delay'].value
        } else {
            delay = ''
        }
        if (delay) {
            style =
                style +
                'animation-delay:' +
                delay +
                ';-webkit-animation-delay:' +
                delay +
                ';'
        }
        b[i].setAttribute('style', style)
    }
}
export function clearSwiperAnimate(a) {
    for (let j = 0; j < a.slides.length; j++)
        for (
            let allBoxes = a.slides[j].querySelectorAll('.ani'), i = 0; i < allBoxes.length; i++
        ) {
            if (allBoxes[i].attributes['swiper-animate-style-cache']) {
                allBoxes[i].setAttribute(
                    'style',
                    allBoxes[i].attributes['swiper-animate-style-cache'].value
                )
            }

            allBoxes[i].style.visibility = 'hidden'
            allBoxes[i].className = allBoxes[i].className.replace('animated', ' ')
            let effect
            if (allBoxes[i].attributes['swiper-animate-effect']) {
                effect = allBoxes[i].attributes['swiper-animate-effect'].value
                allBoxes[i].className = allBoxes[i].className.replace(effect, ' ')
            }
        }

}